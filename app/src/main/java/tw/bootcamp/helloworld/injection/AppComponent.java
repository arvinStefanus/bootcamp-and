package tw.bootcamp.helloworld.injection;

import dagger.Component;
import tw.bootcamp.helloworld.ui.main.MainActivity;

@Component(modules = AppModule.class)
public interface AppComponent {

    void inject(MainActivity mainActivity);
}
