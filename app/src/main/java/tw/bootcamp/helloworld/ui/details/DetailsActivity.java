package tw.bootcamp.helloworld.ui.details;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.squareup.picasso.Picasso;
import tw.bootcamp.helloworld.ui.main.MainActivity;
import tw.bootcamp.helloworld.R;
import tw.bootcamp.helloworld.model.Pokemon;

public class DetailsActivity extends AppCompatActivity implements DetailsView {

    @BindView(R.id.details_name)
    TextView nameView;

    @BindView(R.id.details_weight)
    TextView weightView;

    @BindView(R.id.details_candy)
    TextView candyView;

    @BindView(R.id.details_height)
    TextView heightView;

    @BindView(R.id.details_image)
    ImageView imageView;

    private DetailsPresenter detailsPresenter;

    public DetailsActivity() {
        super();
        detailsPresenter = new DetailsPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Pokemon pokemon = (Pokemon) getIntent().getSerializableExtra(MainActivity.POKEMON_EXTRA);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        detailsPresenter.onCreate(pokemon);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showPokemonDetails(String name, String weight, String height, String candy, String url) {
        nameView.setText(name);
        weightView.setText(weight);
        heightView.setText(height);
        candyView.setText(candy);

        Picasso.with(this).load(url).into(imageView);
    }

    @Override
    public void showTitle(String title) {
        setTitle(title);
    }
}
