package tw.bootcamp.helloworld.ui.main;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import rx.Observable;
import rx.Scheduler;
import tw.bootcamp.helloworld.injection.Schedulers;
import tw.bootcamp.helloworld.model.Pokemon;
import tw.bootcamp.helloworld.service.PokemonService;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MainPresenterTest {

    @Mock
    MainView mainView;

    @Mock
    PokemonService pokemonService;

    @Test
    public void shouldConnectToBackend() throws Exception {
        MainPresenter mainPresenter = new MainPresenter(pokemonService, new TestSchedulers());
        mainPresenter.attachView(mainView);
        List<Pokemon> pokemonList = Arrays.asList(new Pokemon("name", null, null, null, null, 1));
        when(pokemonService.getPokemontList()).thenReturn(Observable.just(
                pokemonList));

        mainPresenter.onCreate();

        verify(mainView).updateTable(pokemonList);
    }

    public class TestSchedulers extends Schedulers {
        @Override
        public Scheduler io() {
            return rx.schedulers.Schedulers.immediate();
        }

        @Override
        public Scheduler main() {
            return rx.schedulers.Schedulers.immediate();
        }
    }
}