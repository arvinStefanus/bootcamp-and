package tw.bootcamp.helloworld.ui.landing;


import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import tw.bootcamp.helloworld.R;
import tw.bootcamp.helloworld.ui.main.MainActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class LandingActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class) {
        @Override
        protected void beforeActivityLaunched() {
            super.beforeActivityLaunched();
            RxIdlingResource.install();
        }
    };

    @Test
    public void landingActivityTest() {
        onView(withText("Bulbasaur")).perform(click());

        onView(withId(R.id.details_candy)).check(matches(withText("Bulbasaur Candy")));
    }

}
