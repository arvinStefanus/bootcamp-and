package tw.bootcamp.helloworld.ui.landing;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.IdlingResource;
import rx.Observable;
import rx.Subscriber;
import rx.plugins.RxJavaObservableExecutionHook;
import rx.plugins.RxJavaPlugins;

import java.util.concurrent.atomic.AtomicInteger;

public final class RxIdlingResource extends RxJavaObservableExecutionHook implements IdlingResource {
    private final AtomicInteger subscriptions = new AtomicInteger(0);

    private static RxIdlingResource INSTANCE;

    private ResourceCallback resourceCallback;

    private RxIdlingResource() {
    }

    public static void install() {
        if (INSTANCE == null) {
            INSTANCE = new RxIdlingResource();
            Espresso.registerIdlingResources(INSTANCE);
            RxJavaPlugins.getInstance().registerObservableExecutionHook(INSTANCE);
        }
    }

    @Override
    public String getName() {
        return RxIdlingResource.class.getName();
    }

    @Override
    public boolean isIdleNow() {
        return subscriptions.get() == 0;
    }

    @Override
    public void registerIdleTransitionCallback(ResourceCallback resourceCallback) {
        this.resourceCallback = resourceCallback;
    }

    @Override
    public <T> Observable.OnSubscribe<T> onSubscribeStart(Observable<? extends T> observableInstance,
                                                          final Observable.OnSubscribe<T> onSubscribe) {
        subscriptions.incrementAndGet();
        return new Observable.OnSubscribe<T>() {
            @Override
            public void call(final Subscriber<? super T> subscriber) {
                onSubscribe.call(new Subscriber<T>() {
                    @Override
                    public void onCompleted() {
                        subscriber.onCompleted();
                        onFinally();
                    }

                    @Override
                    public void onError(Throwable e) {
                        subscriber.onError(e);
                        onFinally();
                    }

                    @Override
                    public void onNext(T t) {
                        subscriber.onNext(t);
                    }
                });
            }
        };
    }

    private void onFinally() {
        if (subscriptions.decrementAndGet() == 0) {
            resourceCallback.onTransitionToIdle();
        }
    }
}